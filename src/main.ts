/**
 * @licence
 * Copyright 2022-2022 - RPG of the Apocalypse
 * Licensed under MIT or any later version
 * Refer to the LICENSE file included.
 */

import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import * as commonServices from "./services/index";

Vue.config.productionTip = false;

// Import common services
Vue.prototype.$services = commonServices;

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
