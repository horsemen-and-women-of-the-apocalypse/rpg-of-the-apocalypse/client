/**
 * @licence
 * Copyright 2022-2022 - RPG of the Apocalypse
 * Licensed under MIT or any later version
 * Refer to the LICENSE file included.
 */

import axios, { AxiosRequestConfig } from "axios";
import { ApiResponse } from "@rpga/common";
import { displayLoadingMessage, clearLoadingMessage } from "@/store/loadingMessages";

const DEFAULT_CONFIG: AxiosRequestConfig = { validateStatus: () => true };

interface RequestParams {
  url: string;
  name?: string;
  config?: AxiosRequestConfig;
}

export class Requester {
  private static async doRequest<T>(func: () => Promise<ApiResponse<T>>, message?: string): Promise<ApiResponse<T>> {
    let reqMessageId;
    try {
      if (message) reqMessageId = displayLoadingMessage(message);
      return await func();
    } finally {
      if (reqMessageId) clearLoadingMessage(reqMessageId);
    }
  }

  public static async get<T>(request: RequestParams): Promise<ApiResponse<T>> {
    return Requester.doRequest(
      async() => (await axios.get<ApiResponse<T>>(request.url, request.config || DEFAULT_CONFIG)).data,
      request.name
    );
  }

  public static async delete<T>(request: RequestParams): Promise<ApiResponse<T>> {
    return Requester.doRequest(
      async() => (await axios.delete<ApiResponse<T>>(request.url, request.config || DEFAULT_CONFIG)).data,
      request.name
    );
  }

  public static async post<T, E>(request: RequestParams, requestPayload: E): Promise<ApiResponse<T>> {
    return Requester.doRequest(
      async() => (await axios.post<ApiResponse<T>>(request.url, requestPayload, request.config || DEFAULT_CONFIG)).data,
      request.name
    );
  }

  public static async put<T, E>(request: RequestParams, requestPayload: E): Promise<ApiResponse<T>> {
    return Requester.doRequest(
      async() => (await axios.put<ApiResponse<T>>(request.url, requestPayload, request.config || DEFAULT_CONFIG)).data,
      request.name
    );
  }

  public static async patch<T, E>(request: RequestParams, requestPayload: E): Promise<ApiResponse<T>> {
    return Requester.doRequest(
      async() => (await axios.patch<ApiResponse<T>>(request.url, requestPayload, request.config || DEFAULT_CONFIG)).data,
      request.name
    );
  }
}
