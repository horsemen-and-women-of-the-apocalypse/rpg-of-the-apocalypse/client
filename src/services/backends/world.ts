/**
 * @licence
 * Copyright 2022-2022 - RPG of the Apocalypse
 * Licensed under MIT or any later version
 * Refer to the LICENSE file included.
 */

import { EntityExport, IngameEntity } from "../editor/entities";
import { SearchOptions } from "./index";
import store from "@/store";
import { MessageType, commit } from "@/store/messages";
import { Requester } from "./axios";

const baseUrl = process.env.VUE_APP_WORLD_SERVICE_URL;

// Maps

interface Map {
  id: string | null;
  name: string;
  readonly tileMap: number[][][]; // To access a tile, tileMap[y][x][layer]
  readonly hitboxMap: number[][]; // To access an hitbox, hitboxMap[y][x]
  readonly entities: EntityExport[];
}

interface DefaultMap extends Map {
  readonly spawnPosition: [number, number];
}

// ==== GET MAPS ====
const getMaps = async(searchOptions: SearchOptions = {}): Promise<(Map | DefaultMap)[]> => {
  // Build request
  let request = `${baseUrl}maps`;

  let limitedRequest;
  if (searchOptions.end !== undefined && searchOptions.start !== undefined) {
    limitedRequest = true;
    request += `?start=${searchOptions.start}`;
    request += `&size=${searchOptions.end - searchOptions.start}`;
  }

  if (searchOptions.search !== undefined) {
    request += `${limitedRequest ? "&" : "?"}search=${searchOptions.search}`;
  }

  console.log(request);

  // Send request
  const response = await Requester.get<(Map | DefaultMap)[]>({
    url: request,
    name: "Loading maps"
  });

  if (response.error !== null) {
    store.commit(commit.display, {
      type: MessageType.Red,
      title: "Error when getting maps",
      text: response.error || "An unknown error occured."
    });
    return [];
  }

  // Find the default map
  const defaultMapId = await getDefaultMapId();
  if (defaultMapId) {
    const defaultMapIndex = response.data.findIndex(
      (map) => map.id === defaultMapId?.mapId);

    if (defaultMapIndex >= 0) {
      response.data[defaultMapIndex] = {
        ...response.data[defaultMapIndex],
        spawnPosition: [ defaultMapId.x, defaultMapId.y ]
      };
    }
  }

  return response.data;
};

const getMap = async(mapId: string): Promise<Map | DefaultMap | undefined> => {
  // Build request
  const request = `${baseUrl}maps/${mapId}`;

  // Send request
  const response = await Requester.get<Map>({
    url: request,
    name: "Loading map"
  });

  if (response.error !== null) {
    store.commit(commit.display, {
      type: MessageType.Red,
      title: "Error when getting the map",
      text: response.error || "An unknown error occured."
    });
    return undefined;
  }

  // Find the default map
  const defaultMapId = await getDefaultMapId();

  if (mapId === defaultMapId?.mapId) {
    // This is the default map
    return {
      ...response.data,
      spawnPosition: [ defaultMapId.x, defaultMapId.y ]
    };
  }

  return response.data;
};

// ==== DEFAULT MAPS ====
interface DefaultMapId {
  x: number,
  y: number;
  mapId: string;
}
const getDefaultMapId = async(): Promise<DefaultMapId | undefined> => {
  // Build request
  const request = `${baseUrl}defaultmap`;

  // Send request
  const response = await Requester.get<DefaultMapId>({ url: request });

  if (response.error !== null) {
    store.commit(commit.display, {
      type: MessageType.Red,
      title: "Error when getting maps",
      text: response.error || "An unknown error occured."
    });
    return undefined;
  }

  return response.data;
};

interface setDefaultMapPayload {
  mapId: string,
  spawn: { x: number, y: number }
}
const setDefaultMap = async(mapId: string, spawnPosition: [number, number]): Promise<void> => {
  const request = `${baseUrl}defaultmap`;
  const response = await Requester.put<string, setDefaultMapPayload>({
    url: request,
    name: "Setting the default map"
  }, {
    mapId,
    spawn: { x: spawnPosition[0], y: spawnPosition[1] }
  });

  if (response.error !== null) {
    store.commit(commit.display, {
      type: MessageType.Red,
      title: "Error when setting the map as default",
      text: response.error || "An unknown error occured."
    });
    return;
  }

  store.commit(commit.display, {
    type: MessageType.Green,
    title: "Default map set",
    text: `The new default map has been successfully set on position ${spawnPosition.join(", ")}.`
  });
};

// ==== SAVE AND DELETE ====
/**
 * Save the map to the world service
 * @param map, the map to save
 */
const saveMap = async(map: Map): Promise<void> => {
  if (map.id !== null) {
    const mapWithSameId = await getMap(map.id);

    if (mapWithSameId) {
      // Replace the map
      const request = `${baseUrl}maps`;
      const response = await Requester.patch<string, Map>({
        url: request,
        name: "Updating map"
      }, map);

      if (response.error !== null) {
        store.commit(commit.display, {
          type: MessageType.Red,
          title: "Error when updating the map",
          text: response.error || "An unknown error occured."
        });
        return;
      }

      store.commit(commit.display, {
        type: MessageType.Green,
        title: "Map updated",
        text: `Map ${map.name} successfully updated`
      });
    } else {
      store.commit(commit.display, {
        type: MessageType.Red,
        title: "Error when getting the map",
        text: "The map that you want to edit doesn't exist anymore"
      });
    }
  } else {
    // Create a new map
    const request = `${baseUrl}maps`;
    const response = await Requester.post<string, Map>({
      url: request,
      name: "Saving map"
    }, map);

    if (response.error !== null) {
      store.commit(commit.display, {
        type: MessageType.Red,
        title: "Error when saving the map",
        text: response.error || "An unknown error occured."
      });
      return;
    }

    store.commit(commit.display, {
      type: MessageType.Green,
      title: "Map saved",
      text: `Map ${map.name} successfully saved`
    });
  }
};

const deleteMap = async(mapId: string): Promise<void> => {
  const request = `${baseUrl}maps/${mapId}`;
  const response = await Requester.delete<string>({
    url: request,
    name: "Deleting map"
  });

  if (response.error !== null) {
    store.commit(commit.display, {
      type: MessageType.Red,
      title: "Error when removing the map",
      text: response.error || "An unknown error occured."
    });
    return;
  }

  store.commit(commit.display, {
    type: MessageType.Green,
    title: "Map deleted",
    text: "The map has been successfully deleted"
  });
};

// ==== GAMES ====
// treated by the world service

interface GameWorld {
  map: Map | DefaultMap;
}

enum Direction {
  North = "North",
  South = "South",
  West = "West",
  East = "East"
}
interface GameUpdate {
  playerPosition: [number, number];
  entities: IngameEntity[];
}

interface GameAnswer {
  mapId: string;
  x: number;
  y: number;
}

const getGame = async(gameId: string): Promise<GameWorld | undefined> => {
  // Build request
  const request = `${baseUrl}games/${gameId}`;

  // Send request
  const response = await Requester.get<GameAnswer>({
    url: request,
    name: "Loading the game"
  });

  if (response.error !== null) {
    store.commit(commit.display, {
      type: MessageType.Red,
      title: "Error when loading the game",
      text: response.error || "An unknown error occured."
    });
    return;
  }

  const map = await getMap(response.data.mapId);
  if (!map) return;

  const defaultMap: DefaultMap = {
    ...map,
    spawnPosition: [ response.data.x, response.data.y ]
  };

  return { map: defaultMap };
};

// ==== IN GAME ====
interface MoveRequestPayload {
  direction: Direction;
  distance: number;
}
interface MoveReturnPayload {
  playerPosition: { x: number, y: number }
}

const move = async(gameId: string, direction: Direction): Promise<GameUpdate | undefined> => {
  // Build request
  const request = `${baseUrl}games/${gameId}/users`;

  // Send request
  const response = await Requester.post<MoveReturnPayload, MoveRequestPayload>({
    url: request,
    name: "Waiting for the game response"
  }, {
    direction,
    distance: 1
  });

  if (response.error !== null) return;

  return {
    playerPosition: [ response.data.playerPosition.x, response.data.playerPosition.y ],
    entities: []
  };
};

// ==== EXPORT ====
export {
  DefaultMap,
  getDefaultMapId,
  Map,
  getMaps,
  getMap,
  saveMap,
  setDefaultMap,
  deleteMap,

  GameWorld,
  getGame,
  move,
  Direction,
  GameUpdate
};
