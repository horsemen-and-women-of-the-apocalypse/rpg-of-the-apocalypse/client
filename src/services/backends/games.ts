/**
 * @licence
 * Copyright 2022-2022 - RPG of the Apocalypse
 * Licensed under MIT or any later version
 * Refer to the LICENSE file included.
 */

import { SearchOptions } from "./index";
import store from "@/store";
import { MessageType, commit } from "@/store/messages";
import { Requester } from "./axios";

function getBaseUrl() : string {
  const userName = store.state.user.user?.username;
  return `${process.env.VUE_APP_GAME_SERVICE_URL}api/v1/${userName}/`;
}

interface Game {
  _id: string;
  gamename: string;
  creationDate: string;
}

const getGames = async(searchOptions: SearchOptions = {}): Promise<Game[]> => {
  // Build request
  let request = `${getBaseUrl()}games`;

  const limitedRequest = searchOptions.end !== undefined && searchOptions.start !== undefined;
  if (limitedRequest) {
    request += `?start=${searchOptions.start}`;
    request += `&end=${searchOptions.end}`;
  }

  if (searchOptions.search !== undefined) {
    request += `${limitedRequest ? "&" : "?"}search=${searchOptions.search}`;
  }

  // Send request
  const response = await Requester.get<Game[]>({
    url: request,
    name: "Loading the games"
  });

  if (response.error !== null) {
    store.commit(commit.display, {
      type: MessageType.Red,
      title: "Error when getting games",
      text: response.error || "An unknown error occured."
    });
    return [];
  }

  return response.data;
};

interface CreateGamePayload {
  gamename: string;
}
/**
 * Create a new game
 * @param gameName That game to add
 * @returns The id of the added game
 */
const createGame = async(gameName: string): Promise<string> => {
  const request = `${getBaseUrl()}games`;
  const response = await Requester.post<string, CreateGamePayload>({
    url: request,
    name: "Creating the game"
  }, {
    gamename: gameName
  });

  if (response.error !== null) {
    store.commit(commit.display, {
      type: MessageType.Red,
      title: "Error when creating the game",
      text: response.error || "An unknown error occured."
    });
    return "";
  }
  store.commit(commit.display, {
    type: MessageType.Green,
    title: "Game created",
    text: `The game '${gameName}' has been successfully created.`
  });

  return response.data;
};

const deleteGame = async(gameId: string): Promise<void> => {
  const request = `${getBaseUrl()}games/${gameId}`;
  const response = await Requester.delete<boolean>({
    url: request,
    name: "Deleting the game"
  });

  if (response.error !== null) {
    store.commit(commit.display, {
      type: MessageType.Red,
      title: "Error when deleting the game",
      text: response.error || "An unknown error occured."
    });
    return;
  }

  store.commit(commit.display, {
    type: MessageType.Green,
    title: "Game deleted",
    text: "The game has been successfully deleted."
  });
};

export {
  Game,
  getGames,
  createGame,
  deleteGame
};
