/**
 * @licence
 * Copyright 2022-2022 - RPG of the Apocalypse
 * Licensed under MIT or any later version
 * Refer to the LICENSE file included.
 */

import * as users from "./users";
import * as world from "./world";
import * as games from "./games";

interface SearchOptions {
  start?: number;
  end?: number;
  search?: string;
}

export {
  SearchOptions,
  users,
  games,
  world
};
