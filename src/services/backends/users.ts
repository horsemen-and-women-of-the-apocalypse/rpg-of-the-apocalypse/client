/**
 * @licence
 * Copyright 2022-2022 - RPG of the Apocalypse
 * Licensed under MIT or any later version
 * Refer to the LICENSE file included.
 */

import { SearchOptions } from "./index";
import store from "@/store";
import { MessageType, commit } from "@/store/messages";
import { setToken, setUser, logout as storeLogout } from "@/store/user";
import { Requester } from "./axios";

const baseUrl = process.env.VUE_APP_USER_SERVICE_URL;

export enum UserRole {
  ADMIN = "ADMIN",
  USER = "USER"
}

interface User {
  username: string;
  role: UserRole;
}

// === Auth ===
// eslint-disable-next-line @typescript-eslint/no-unused-vars
const login = async(username: string, password: string): Promise<User | undefined> => {
  // tmp
  if (username.length === 0) {
    store.commit(commit.display, {
      type: MessageType.Red,
      title: "Error when logging in",
      text: "Please enter a username and a password."
    });
    return;
  }

  const user = await getUser(username);

  if (user === undefined) {
    store.commit(commit.display, {
      type: MessageType.Red,
      title: "Error when logging in",
      text: "The user does not exist."
    });
    return;
  }

  setToken("FakeToken");
  setUser(user);

  return user;
  // // Build request
  // const request = `${baseUrl}login`;

  // // Send request
  // const response = await Requester.post<string>({
  //   url: request,
  //   name: "Logging in",
  //   data: {
  //     username,
  //     password
  //   }
  // });

  // if (response.error !== null) {
  //   store.commit(commit.display, {
  //     type: MessageType.Red,
  //     title: "Error when logging in",
  //     text: response.error || "An unknown error occured."
  //   });
  //   return "";
  // }

  // return response.data;
};

const logout = async(): Promise<void> => {
  // TODO: call the backend to logout
  storeLogout();
};

// === Users ===
const getUsers = async(searchOptions: SearchOptions, role: string): Promise<User[]> => {
  // Build request
  let request = `${baseUrl}users?role=${role}`;

  if (searchOptions.end !== undefined && searchOptions.start !== undefined) {
    request += `&limit=${searchOptions.end - searchOptions.start}`;

    if (searchOptions.start > 0) {
      request += `&offset=${searchOptions.start}`;
    }
  }

  if (searchOptions.search !== undefined) {
    request += `&username=${searchOptions.search}`;
  }

  // Send request
  const response = await Requester.get<User[]>({
    url: request,
    name: "Loading the users"
  });

  if (response.error !== null) {
    store.commit(commit.display, {
      type: MessageType.Red,
      title: "Error when getting users",
      text: response.error || "An unknown error occured."
    });
    return [];
  }

  return response.data;
};

const getUser = async(userName: string): Promise<User | undefined> => {
  // tmp
  return userName === "admin" ? {
    username: "admin",
    role: UserRole.ADMIN
  } : {
    username: userName,
    role: UserRole.USER
  };

  // // Build request
  // const request = `${baseUrl}users/${userName}`;

  // // Send request
  // const response = await Requester.get<User>({ url: request });

  // if (response.error !== null) {
  //   store.commit(commit.display, {
  //     type: MessageType.Red,
  //     title: "Error when getting users",
  //     text: response.error || "An unknown error occured."
  //   });
  //   return [];
  // }

  // return response.data;
};

const getNonAdminUsers = async(searchOptions: SearchOptions = {}): Promise<User[]> => {
  return await getUsers(searchOptions, "USER");
};

const getAdminUsers = async(searchOptions: SearchOptions): Promise<User[]> => {
  return await getUsers(searchOptions, "ADMIN");
};

interface CreateUserPayload {
  username: string;
  role: string;
}

const addUser = async(user: User): Promise<string> => {
  const request = `${baseUrl}users`;
  const response = await Requester.post<string, CreateUserPayload>({
    url: request,
    name: "Adding the user"
  }, {
    username: user.username,
    role: user.role
  });

  if (response.error !== null) {
    store.commit(commit.display, {
      type: MessageType.Red,
      title: "Error when adding the user",
      text: response.error || "An unknown error occured."
    });
    return "";
  }

  store.commit(commit.display, {
    type: MessageType.Green,
    title: "User added",
    text: `The ${user.role} ${user.username} has been successfully added.`
  });

  return response.data;
};

const banUser = async(username: string): Promise<void> => {
  const request = `${baseUrl}users/${username}`;
  const response = await Requester.delete<boolean>({
    url: request,
    name: "Banning the user"
  });

  if (response.error !== null) {
    store.commit(commit.display, {
      type: MessageType.Red,
      title: "Error when removing the user",
      text: response.error || "An unknown error occured."
    });
    return;
  }

  store.commit(commit.display, {
    type: MessageType.Green,
    title: "User removed",
    text: `${username} has been successfully removed.`
  });
};

export {
  User,
  login,
  logout,

  getNonAdminUsers,
  getAdminUsers,
  addUser,
  banUser
};
