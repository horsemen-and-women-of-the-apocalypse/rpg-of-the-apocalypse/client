/**
 * @licence
 * Copyright 2022-2022 - RPG of the Apocalypse
 * Licensed under MIT or any later version
 * Refer to the LICENSE file included.
 */

import { Tileset, tilesets } from "./tilesets";
import Phaser from "phaser";
// Available entities :
// Currently, the available entity type are : "monster" and "item"

interface KnownEntity {
  readonly name: string;
  readonly type: string;
  readonly tileset: Tileset;
  readonly imageTilesetCoord: [number, number];
  readonly imagePxYOffset?: number;
  readonly idleAnimationImagesCoord?: [number, number][];
  readonly runningAnimationImagesCoord?: [number, number][];
}

interface Entity extends KnownEntity {
  readonly imageTilesetPosition: number;
  readonly idleAnimationImagesPosition?: number[];
  readonly runningAnimationImagesPosition?: number[];
}

interface IngameEntity extends Entity {
  readonly id: string;
  x: number;
  y: number;
}

interface DisplayedEntity extends IngameEntity {
  readonly sprite: Phaser.Types.Physics.Arcade.SpriteWithDynamicBody | Phaser.GameObjects.Sprite;
  readonly idleAnimation?: Phaser.Animations.Animation
  readonly runningAnimation?: Phaser.Animations.Animation
}

interface EntityExport {
  readonly name: string;
  readonly x: number;
  readonly y: number;
}

const _knownEntities: KnownEntity[] = [
  {
    name: "player",
    type: "player",
    tileset: tilesets.entities,
    imageTilesetCoord: [ 0, 2 ],
    idleAnimationImagesCoord: [
      [ 0, 2 ],
      [ 1, 2 ],
      [ 2, 2 ],
      [ 3, 2 ]
    ],
    runningAnimationImagesCoord: [
      [ 4, 2 ],
      [ 5, 2 ],
      [ 6, 2 ],
      [ 7, 2 ]
    ]
  },
  {
    name: "trap_1",
    type: "item",
    tileset: tilesets.items,
    imageTilesetCoord: [ 0, 0 ]
  },
  {
    name: "trap_2",
    type: "item",
    tileset: tilesets.items,
    imageTilesetCoord: [ 1, 0 ],
    idleAnimationImagesCoord: [
      [ 1, 0 ],
      [ 1, 0 ],
      [ 2, 0 ],
      [ 2, 0 ]
    ]
  },
  {
    name: "trap_3",
    type: "item",
    tileset: tilesets.items,
    imageTilesetCoord: [ 3, 0 ]
  },
  {
    name: "chest",
    type: "item",
    tileset: tilesets.items,
    imageTilesetCoord: [ 0, 1 ],
    imagePxYOffset: 4
  },
  {
    name: "devil",
    type: "monster",
    tileset: tilesets.entities,
    imageTilesetCoord: [ 0, 13 ],
    idleAnimationImagesCoord: [
      [ 0, 13 ],
      [ 1, 13 ],
      [ 2, 13 ],
      [ 3, 13 ]
    ],
    runningAnimationImagesCoord: [
      [ 4, 13 ],
      [ 5, 13 ],
      [ 6, 13 ],
      [ 7, 13 ]
    ]
  }
];

const getTilesetPosFromCoord = (tileset: Tileset, coord: [number, number]): number => {
  const [ x, y ] = coord;
  return x + y * tileset.nbImagesPerRow;
};

// Set the imageTilesetPosition to the correct value
const knownEntities: { [key: string]: Entity } = {};

Object.values(_knownEntities).forEach(entity => {
  knownEntities[entity.name] = {
    ...entity,
    imageTilesetPosition: getTilesetPosFromCoord(
      entity.tileset,
      entity.imageTilesetCoord
    ),
    idleAnimationImagesPosition: entity.idleAnimationImagesCoord
      ? entity.idleAnimationImagesCoord.map(
        coord => getTilesetPosFromCoord(entity.tileset, coord))
      : undefined,
    runningAnimationImagesPosition: entity.runningAnimationImagesCoord
      ? entity.runningAnimationImagesCoord.map(
        coord => getTilesetPosFromCoord(entity.tileset, coord))
      : undefined
  };
});

export {
  Entity,
  EntityExport,
  IngameEntity,
  DisplayedEntity,
  knownEntities
};
