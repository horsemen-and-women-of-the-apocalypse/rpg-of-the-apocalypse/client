/**
 * @licence
 * Copyright 2022-2022 - RPG of the Apocalypse
 * Licensed under MIT or any later version
 * Refer to the LICENSE file included.
 */

interface Tileset {
    readonly name: string;
    readonly path: string;
    readonly nbImagesPerRow: number;
    readonly imageSize: [number, number];
}

const tilesets: { [key: string]: Tileset} = {
  tiles: {
    name: "tiles",
    path: "/assets/tilesets/tiles.png",
    nbImagesPerRow: 12,
    imageSize: [ 16, 16 ]
  },
  items: {
    name: "items",
    path: "/assets/tilesets/items.png",
    nbImagesPerRow: 4,
    imageSize: [ 16, 16 ]
  },
  entities: {
    name: "entities",
    path: "/assets/tilesets/entities.png",
    nbImagesPerRow: 9,
    imageSize: [ 16, 32 ]
  }
};

function tileCoordToTileIndex(x: integer, y: integer, tileset: Tileset): integer {
  return x + tileset.nbImagesPerRow * y;
}

export { Tileset, tilesets, tileCoordToTileIndex };
