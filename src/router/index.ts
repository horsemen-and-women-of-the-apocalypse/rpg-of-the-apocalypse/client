/**
 * @licence
 * Copyright 2022-2022 - RPG of the Apocalypse
 * Licensed under MIT or any later version
 * Refer to the LICENSE file included.
 */

import Vue from "vue";
import VueRouter from "vue-router";
import { UserRole } from "@/services/backends/users";
import { commit, MessageType } from "@/store/messages";
import store from "@/store";

import Login from "../components/Login.vue";
import Games from "../components/player/Games.vue";
import Board from "../components/player/board/GameBoard.vue";
import Admin from "../components/admin/AdminPage.vue";
import MapEditor from "../components/admin/mapEditor/MapEditor.vue";
import { RouteConfigSingleView } from "vue-router/types/router";

Vue.use(VueRouter);
interface Route extends RouteConfigSingleView {
  loginIsRequired: boolean;
  restrictedRole?: UserRole;
}

const routes: Array<Route> = [
  {
    path: "/",
    name: "Login",
    component: Login,
    loginIsRequired: false
  },
  // === User routes ===
  {
    path: "/games",
    name: "Games",
    component: Games,
    loginIsRequired: true,
    restrictedRole: UserRole.USER
  },
  {
    path: "/board",
    name: "Board",
    component: Board,
    loginIsRequired: true,
    restrictedRole: UserRole.USER
  },
  // === Admin routes ===
  {
    path: "/admin",
    name: "Admin",
    component: Admin,
    loginIsRequired: true,
    restrictedRole: UserRole.ADMIN
  },
  {
    path: "/admin/maps/editor",
    name: "MapEditor_NewMap",
    component: MapEditor,
    loginIsRequired: true,
    restrictedRole: UserRole.ADMIN
  },
  {
    path: "/admin/maps/editor/:mapId",
    name: "MapEditor_EditMap",
    component: MapEditor,
    loginIsRequired: true,
    restrictedRole: UserRole.ADMIN
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

router.beforeEach((to, from, next) => {
  if (!to.name) {
    store.commit(commit.display, {
      type: MessageType.Red,
      title: "Error when navigating"
    });
    return next("/");
  }

  const route = routes.find(r => r.name === to.name);

  if (!route) {
    store.commit(commit.display, {
      type: MessageType.Red,
      title: "Unknown route"
    });
    return next("/");
  }

  // Check if user is logged in
  if (route.loginIsRequired && !store.state.user.user) {
    store.commit(commit.display, {
      type: MessageType.Red,
      title: "You need to be logged in to access this page"
    });
    return next("/");
  }

  // Check if user has the right role
  if (route.restrictedRole &&
    store.state.user.user &&
    store.state.user.user.role !== route.restrictedRole) {
    store.commit(commit.display, {
      type: MessageType.Red,
      title: "You do not have the required permissions to access this page"
    });
    return next(from.path);
  }

  next();
});

export default router;
