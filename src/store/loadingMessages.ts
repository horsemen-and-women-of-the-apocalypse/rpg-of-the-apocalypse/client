/**
 * @licence
 * Copyright 2022-2022 - RPG of the Apocalypse
 * Licensed under MIT or any later version
 * Refer to the LICENSE file included.
 */

import { v4 as uuid } from "uuid";
import store from "@/store";

type messageId = string;

export interface LoadingMessage {
  id: messageId;
  text: string;
}

export interface LoadingMessagesState {
  messages: LoadingMessage[]
}

export const commit = {
  display: "loadingMessages/display",
  clear: "loadingMessages/clear"
};

// Exported functions
export function displayLoadingMessage(text: string): string {
  // We need to go through this function because store commit can't
  // return a value.

  // Generate an id for the message
  const messageId = uuid();
  store.commit(commit.display, { id: messageId, text });
  // return the id of the message
  return messageId;
}

export function clearLoadingMessage(id: messageId): void {
  store.commit(commit.clear, id);
}

// Store module
export default {
  namespaced: true,
  state: (): LoadingMessagesState => ({
    messages: []
  }),

  mutations: {
    display: function(state: LoadingMessagesState, message: LoadingMessage): void {
      // Store the message
      state.messages.push(message);
    },

    clear: function(state: LoadingMessagesState, messageId: messageId): void {
      state.messages = state.messages.filter(m => m.id !== messageId);
    }
  }
};
