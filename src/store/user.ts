/**
 * @licence
 * Copyright 2022-2022 - RPG of the Apocalypse
 * Licensed under MIT or any later version
 * Refer to the LICENSE file included.
 */

import { User } from "@/services/backends/users";
import store from ".";

export interface UserState {
  user: User | null;
  token: string | null;
}

export const commit = {
  setUser: "user/setUser",
  setToken: "user/setToken",
  logout: "user/logout"
};

export function setUser(user: User): void { store.commit(commit.setUser, user); }
export function setToken(token: string): void { store.commit(commit.setToken, token); }
export function logout(): void { store.commit(commit.logout); }

export default {
  namespaced: true,
  state: (): UserState => ({
    user: null,
    token: null
  }),

  mutations: {
    setUser: function(state: UserState, user: User): void {
      state.user = user;
    },

    setToken: function(state: UserState, token: string): void {
      state.token = token;
    },

    logout: function(state: UserState): void {
      state.user = null;
      state.token = null;
    }

  }
};
