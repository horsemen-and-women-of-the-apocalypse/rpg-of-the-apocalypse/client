/**
 * @licence
 * Copyright 2022-2022 - RPG of the Apocalypse
 * Licensed under MIT or any later version
 * Refer to the LICENSE file included.
 */

import { v4 as uuid } from "uuid";

export enum MessageType {
  Green = "green",
  Red = "red",
  Yellow = "yellow",
  White = "white",
}

export interface Message {
  id?: string;
  type: MessageType;
  title: string;
  text: string;
}

export interface MessagesState {
  messages: Message[]
}

export const commit = {
  display: "messages/display",
  clear: "messages/clear"
};

const messageExpirationTime = 4000;

export default {
  namespaced: true,
  state: (): MessagesState => ({
    messages: []
  }),

  mutations: {
    display: function(state: MessagesState, message: Message): void {
      // Generate an id for the message
      const messageId = uuid();
      message.id = messageId;

      // Store the message
      state.messages.push(message);

      // Remove messages after a certain time
      setTimeout(() => {
        state.messages = state.messages.filter(m => m.id !== messageId);
      }, messageExpirationTime);
    },

    clear: function(state: MessagesState, messageId: string): void {
      state.messages = state.messages.filter(m => m.id !== messageId);
    }
  }
};
