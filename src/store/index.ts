/**
 * @licence
 * Copyright 2022-2022 - RPG of the Apocalypse
 * Licensed under MIT or any later version
 * Refer to the LICENSE file included.
 */

import messageModule, { MessagesState } from "./messages";
import loadingMessageModule, { LoadingMessagesState } from "./loadingMessages";
import userModule, { UserState } from "./user";

import Vue from "vue";
import Vuex from "vuex";

interface State {
  messages: MessagesState;
  loadingMessages: LoadingMessagesState;
  user: UserState;
}

Vue.use(Vuex);
export default new Vuex.Store<State>({
  modules: {
    messages: messageModule,
    loadingMessages: loadingMessageModule,
    user: userModule
  }
});
