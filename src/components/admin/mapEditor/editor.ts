/**
 * @licence
 * Copyright 2022-2022 - RPG of the Apocalypse
 * Licensed under MIT or any later version
 * Refer to the LICENSE file included.
 */

import Phaser from "phaser";
import { v4 as uuidv4 } from "uuid";
import { Map, DefaultMap } from "../../../services/backends/world";
import { DisplayedEntity, knownEntities } from "../../../services/editor/entities";// Tileset parameters
import { tilesets, tileCoordToTileIndex } from "../../../services/editor/tilesets";// Tileset parameters

// Tileset parameters
const TILESET = tilesets.tiles;
const TILE_SIZE_PX = TILESET.imageSize[0];

const EMPTY_TILE_INDEX = tileCoordToTileIndex(0, 4, TILESET);
const DEFAULT_TILE = tileCoordToTileIndex(7, 8, TILESET);

// Editor parameters
const CAMERA_SPEED = 6;
const NUMBER_OF_LAYERS = 5;
const SELECTED_TILE_CHANGED_EVENT_NAME = "tileToPaintChanged";
const EDITOR_CREATED_EVENT_NAME = "editorCreated";
const NEW_SPAWN_EVENT_NAME = "spawnPlaced";

class EditorScene extends Phaser.Scene {
  // Editor
  private MAP_SIZE = [ 25, 20 ];
  private get MAP_SIZE_PX_X(): integer {
    return this.MAP_SIZE[0] * TILE_SIZE_PX;
  }

  private get MAP_SIZE_PX_Y(): integer {
    return this.MAP_SIZE[1] * TILE_SIZE_PX;
  }

  private map?: Phaser.Tilemaps.Tilemap;
  private marker?: Phaser.GameObjects.Graphics;
  private borderRectangle?: Phaser.GameObjects.Rectangle;
  private layers: Phaser.Tilemaps.TilemapLayer[] = [];
  private hitboxTiles: (Phaser.GameObjects.Rectangle | null)[][] = [];
  private entities: DisplayedEntity[] = [];
  private tileToPaintIndex = DEFAULT_TILE;
  private selectedLayer: number | null = 0;
  private selectedEntityName: string | null = null;
  private spawnPositionRect?: Phaser.GameObjects.Rectangle;
  private spawnPosition?: [number, number];
  private spawnPositionMode = false;

  // Controls
  private cursors?: Phaser.Types.Input.Keyboard.CursorKeys;
  private spaceBarKey?: Phaser.Input.Keyboard.Key;
  private hitboxMode = false;

  // Init
  constructor() {
    super("EditorScene");
  }

  preload(): void {
    // Load tilesets
    Object.values(tilesets).forEach(tileset => {
      this.load.spritesheet(tileset.name, tileset.path, {
        frameWidth: tileset.imageSize[0],
        frameHeight: tileset.imageSize[1]
      });
    });
  }

  create(): void {
    // Create the editor map
    this.resetMap();

    // Create a marker to show the position of the cursor
    this.marker = this.add.graphics();
    this.marker.strokeRect(0, 0, TILE_SIZE_PX, TILE_SIZE_PX);
    this.marker.lineStyle(1, 0xffffff, 1);
    this.marker.setDepth(100);
    this.marker.setVisible(false);

    // Camera
    this.cameras.main.scrollX = -this.MAP_SIZE_PX_X * 1.5;
    this.cameras.main.scrollY = -this.MAP_SIZE_PX_Y * 1.3;
    this.cameras.main.setZoom(3);
    // this.cameras.main.setBounds(
    //   -TILE_SIZE_PX * 20,
    //   -TILE_SIZE_PX * 20,
    //   (40 + this.MAP_SIZE[0]) * TILE_SIZE_PX,
    //   (40 + this.MAP_SIZE[1]) * TILE_SIZE_PX
    // );

    // Input
    this.input.mouse.disableContextMenu();
    this.cursors = this.input.keyboard.createCursorKeys();
    this.spaceBarKey = this.input.keyboard.addKey(
      Phaser.Input.Keyboard.KeyCodes.SPACE
    );

    // Mouse middle click
    this.input.on("pointerdown", (pointer: Phaser.Input.Pointer) =>
      this.dealWithMouseClick(pointer)
    );
    // Mouse wheel
    this.input.on(
      "wheel",
      (p: Phaser.Input.Pointer, dX: number, dY: number, deltaZ: number) =>
        this.dealWithMouseWheel(deltaZ)
    );
    // Map drag
    this.input.on("pointermove", (pointer: Phaser.Input.Pointer) =>
      this.dealWithMouseDrag(pointer)
    );

    // Tell the editor our default tile
    this.events.emit(SELECTED_TILE_CHANGED_EVENT_NAME, this.tileToPaintIndex);
    this.events.emit(EDITOR_CREATED_EVENT_NAME);
  }

  resetMap(): void {
    // Creating an empty map
    this.map = this.make.tilemap({
      tileWidth: TILE_SIZE_PX,
      tileHeight: TILE_SIZE_PX,
      width: this.MAP_SIZE[0],
      height: this.MAP_SIZE[1]
    });

    // Load tiles and reset layers
    const tiles = this.map.addTilesetImage(
      TILESET.name,
      undefined,
      TILE_SIZE_PX,
      TILE_SIZE_PX,
      0,
      0
    );

    // Reset layers
    this.layers.forEach(layer => {
      layer.destroy();
    });

    this.layers = [];
    for (let i = 0; i < NUMBER_OF_LAYERS; i++) {
      this.layers.push(this.map.createBlankLayer(`layer${i}`, tiles));
      // Fill the layer with empty tiles
      this.layers[i].fill(EMPTY_TILE_INDEX, 0, 0, this.MAP_SIZE[0], this.MAP_SIZE[1]);
    }

    // Reset entities
    this.entities.forEach(entity => entity.sprite.destroy());
    this.entities = [];

    // Reset the hitbox tiles
    this.hitboxTiles.forEach((row) => {
      row.forEach((tile) => {
        if (tile !== null) tile.destroy();
      });
    });
    this.hitboxTiles = [];
    for (let i = 0; i < this.MAP_SIZE[0]; i++) {
      this.hitboxTiles.push([]);
      for (let j = 0; j < this.MAP_SIZE[1]; j++) this.hitboxTiles[i].push(null);
    }
    this.hitboxMode = false;

    // Draw rectangle around the map
    this.borderRectangle?.destroy();
    this.borderRectangle = this.add.rectangle(
      this.MAP_SIZE_PX_X / 2,
      this.MAP_SIZE_PX_Y / 2,
      this.MAP_SIZE_PX_X,
      this.MAP_SIZE_PX_Y
    );
    this.borderRectangle.setStrokeStyle(1, 0xffffff);

    // Reset spawn position
    this.spawnPositionRect?.destroy();
  }

  // Controls
  dealWithMouseClick(pointer: Phaser.Input.Pointer): void {
    if (this.map && this.selectedLayer !== null && this.input.isOver) {
      const selectedTile = this.layers[this.selectedLayer].getTileAtWorldXY(
        this.input.activePointer.worldX,
        this.input.activePointer.worldY
      );
      if (selectedTile && pointer.middleButtonDown()) {
        this.copyTile(selectedTile);
      }
    }
  }

  dealWithMouseWheel(delta: number): void {
    this.cameras.main.zoomTo(
      Math.max(1, this.cameras.main.zoom + (delta > 0 ? -1 : 1)),
      100
    );
  }

  dealWithMouseDrag(pointer: Phaser.Input.Pointer): void {
    if (this.spaceBarKey?.isDown) {
      this.input?.manager.setDefaultCursor("grab");

      if (pointer.isDown) {
        this.input?.manager.setDefaultCursor("grabbing");
        this.cameras.main.scrollX -=
          (pointer.x - pointer.prevPosition.x) / this.cameras.main.zoom;
        this.cameras.main.scrollY -=
          (pointer.y - pointer.prevPosition.y) / this.cameras.main.zoom;
      }
    } else {
      this.input?.manager.setDefaultCursor("default");
    }
  }

  // Actions
  copyTile(selectedTile: Phaser.Tilemaps.Tile): void {
    if (this.tileToPaintIndex !== selectedTile.index) {
      this.tileToPaintIndex = selectedTile.index;
      this.events.emit(SELECTED_TILE_CHANGED_EVENT_NAME, this.tileToPaintIndex);
    }
  }

  placeTile(selectedTile: Phaser.Tilemaps.Tile): void {
    if (selectedTile.x < 0 || selectedTile.x >= this.MAP_SIZE[0] ||
      selectedTile.y < 0 || selectedTile.y >= this.MAP_SIZE[1]) return;

    if (this.selectedLayer !== null) {
      // If the selected layer is not empty, we place a tile
      this.layers[this.selectedLayer].putTileAt(
        this.tileToPaintIndex,
        selectedTile.x,
        selectedTile.y
      );
    } else if (
      this.hitboxMode &&
      this.hitboxTiles[selectedTile.x][selectedTile.y] === null
    ) {
      // Hitbox mode
      this.placeHitboxTile(selectedTile.x, selectedTile.y);
    } else if (this.selectedEntityName) {
      // Entity mode
      this.placeEntity(selectedTile.x, selectedTile.y, this.selectedEntityName);
    } else if (this.spawnPositionMode) {
      // Spawn position mode
      this.placeSpawnPosition([ selectedTile.x, selectedTile.y ]);
      this.events.emit(NEW_SPAWN_EVENT_NAME, [ selectedTile.x, selectedTile.y ]);
    }
  }

  placeHitboxTile(x: number, y: number): void {
    const rectangle = this.add.rectangle(
      x * TILE_SIZE_PX + TILE_SIZE_PX / 2,
      y * TILE_SIZE_PX + TILE_SIZE_PX / 2,
      TILE_SIZE_PX,
      TILE_SIZE_PX
    );
    rectangle.setFillStyle(0xff0000, 0.3);
    this.hitboxTiles[x][y] = rectangle;
  }

  placeEntity(x: number, y: number, entityName: string): void {
    // Get the entity that we want to place
    const entity = knownEntities[entityName];
    if (entity === undefined) return;

    // Check if the entity is already placed
    // At the moment, we allow multiple entity per tile
    const alreadyPlaced = this.entities.find(
      entity => entity.x === x && entity.y === y && entity.name === entityName
    );
    if (alreadyPlaced !== undefined) return;

    // Create the entity
    const entitySprite = this.physics.add.sprite(
      x * TILE_SIZE_PX + TILE_SIZE_PX / 2,
      y * TILE_SIZE_PX + TILE_SIZE_PX / 2,
      entity.tileset.name,
      entity.imageTilesetPosition
    );

    this.entities.push({
      ...entity,
      id: uuidv4(),
      name: entityName,
      x: x,
      y: y,
      sprite: entitySprite
    });
  }

  removeTile(selectedTile: Phaser.Tilemaps.Tile): void {
    if (this.selectedLayer !== null) {
      this.layers[this.selectedLayer].putTileAt(
        EMPTY_TILE_INDEX,
        selectedTile.x,
        selectedTile.y
      );
    } else if (this.hitboxTiles[selectedTile.x][selectedTile.y]) {
      // Hitbox mode
      this.hitboxTiles[selectedTile.x][selectedTile.y]?.destroy();
      this.hitboxTiles[selectedTile.x][selectedTile.y] = null;
    } else if (this.selectedEntityName !== null) {
      // Entity mode
      // Removing the entity even if it is not the selected one
      const placedEntity = this.entities.find(
        entity => entity.x === selectedTile.x &&
          entity.y === selectedTile.y
      );
      if (placedEntity === undefined) return;

      placedEntity.sprite.destroy();
      this.entities = this.entities.filter(
        entity => entity.id !== placedEntity.id
      );
    }
  }

  // Run
  update(): void {
    this.marker?.setVisible(false);
    if (this.input.isOver && (
      this.selectedLayer !== null ||
      this.hitboxMode ||
      this.selectedEntityName !== null ||
      this.spawnPositionMode
    )) {
      // Get the tile at the pointer position
      const tile = this.layers[0].getTileAtWorldXY(
        this.input.activePointer.worldX,
        this.input.activePointer.worldY
      );

      if (tile) {
        // Update the marker position
        this.marker?.setVisible(true);
        this.marker?.setPosition(tile.x * TILE_SIZE_PX, tile.y * TILE_SIZE_PX);

        // Deal with dragged mouse click
        if (!this.spaceBarKey || !this.spaceBarKey.isDown) {
          if (this.input.activePointer.leftButtonDown()) this.placeTile(tile);
          else if (this.input.activePointer.rightButtonDown()) this.removeTile(tile);
        } else {
          this.marker?.setVisible(false);
        }
      }
    }

    // Camera controls
    if (this.cursors) {
      if (this.cursors.up.isDown) {
        this.cameras.main.y += CAMERA_SPEED;
      } else if (this.cursors.down.isDown) {
        this.cameras.main.y -= CAMERA_SPEED;
      }

      if (this.cursors.left.isDown) {
        this.cameras.main.x += CAMERA_SPEED;
      } else if (this.cursors.right.isDown) {
        this.cameras.main.x -= CAMERA_SPEED;
      }
    }
  }

  // Exported methods
  public setTileToPaint(tileIndex: number): void {
    this.tileToPaintIndex = tileIndex;
  }

  public setLayer(layerNumber: number | null): void {
    // Set all the other layers to a low alpha
    // except the selected one
    let defaultAlpha = 0.5;
    if (layerNumber === null) {
      // no layer selected, set all layers to default full alpha
      this.selectedLayer = layerNumber;
      defaultAlpha = 1;
    } else {
      this.selectedLayer = Math.max(0, Math.min(NUMBER_OF_LAYERS, layerNumber));
    }

    for (let i = 0; i < NUMBER_OF_LAYERS; i++) {
      this.layers[i].setAlpha(i === this.selectedLayer ? 1 : defaultAlpha);
    }
  }

  public setHitboxMode(state: boolean): void {
    this.hitboxMode = state;
    if (!this.hitboxMode) {
      // Hide all hitboxe tiles
      for (let x = 0; x < this.hitboxTiles.length; x++) {
        for (let y = 0; y < this.hitboxTiles[x].length; y++) {
          this.hitboxTiles[x][y]?.setVisible(false);
        }
      }
    } else {
      // Show all hitboxe tiles
      for (let x = 0; x < this.hitboxTiles.length; x++) {
        for (let y = 0; y < this.hitboxTiles[x].length; y++) {
          this.hitboxTiles[x][y]?.setVisible(true);
        }
      }
    }
  }

  public setSpawnPositionMode(state: boolean): void {
    this.spawnPositionMode = state;
  }

  public setEntity(entityName: string | null): void {
    this.selectedEntityName = entityName;
    if (!this.selectedEntityName) {
      // Hide all entities a bit
      this.entities.forEach(entity => entity.sprite.setAlpha(0.2));
    } else {
      // Show all entities
      this.entities.forEach(entity => entity.sprite.setAlpha(1));
    }
  }

  public getMap(): Map | DefaultMap {
    // To access a tile, tileMap[y][x][layer]
    // Creation of a 3D array
    const tileMap = [ ...Array(this.MAP_SIZE[1]) ]
      .map(() => [ ...Array(this.MAP_SIZE[0]) ]
        .map(() => [ ...Array(NUMBER_OF_LAYERS) ]));

    // Convert layers to tileMap
    this.layers.forEach((layer, layerNumber) => {
      layer.getTilesWithin().forEach(tile => {
        tileMap[tile.y][tile.x][layerNumber] = tile.index;
      });
    });

    // Convert hitboxMap
    const hitboxMap = [ ...Array(this.MAP_SIZE[1]) ]
      .map(() => [ ...Array(this.MAP_SIZE[0])
        .fill(0) ]);

    this.hitboxTiles.forEach((col, x) => {
      col.forEach((tile, y) => {
        if (tile) hitboxMap[y][x] = 1;
      });
    });

    // Convert entities
    const entities = this.entities.map(entity => {
      return {
        name: entity.name,
        x: entity.x,
        y: entity.y
      };
    });

    const ret = {
      name: "",
      id: null,
      tileMap,
      hitboxMap,
      entities
    };

    // Add spawn position
    if (this.spawnPosition) {
      return {
        ...ret,
        spawnPosition: this.spawnPosition
      };
    }

    return ret;
  }

  public setMap(map: Map | DefaultMap): void {
    // Reset the map
    this.MAP_SIZE = [ map.tileMap[0].length, map.tileMap.length ];
    this.resetMap();
    this.applyMap(map);
  }

  public setMapSize(sizeX: number, sizeY: number): void {
    // Save the current map
    const currentMap = this.getMap();

    // Reset the map
    this.MAP_SIZE = [ sizeX, sizeY ];
    this.resetMap();

    // Load the tiles and the hitboxes that we can
    this.applyMap(currentMap);
  }

  private applyMap(map: Map | DefaultMap): void {
    // Set tileMap
    map.tileMap.forEach((row, y) => {
      row.forEach((layerTiles, x) => {
        layerTiles.forEach((layerTile, layerNumber) => {
          this.layers[layerNumber].putTileAt(layerTile, x, y);
        });
      });
    });

    // Set hitboxMap
    map.hitboxMap.forEach((row, y) => {
      row.forEach((tile, x) => {
        if (tile) this.placeHitboxTile(x, y);
      });
    });

    // Hide the hitbox tiles
    this.setHitboxMode(false);

    // Set entities
    if (map.entities) {
      map.entities.forEach(entity => {
        if (entity.x < this.MAP_SIZE[0] && entity.y < this.MAP_SIZE[1]) {
          this.placeEntity(entity.x, entity.y, entity.name);
        }
      });
    }

    // Set spawn position
    if ("spawnPosition" in map) {
      this.placeSpawnPosition(map.spawnPosition);
    }
  }

  private placeSpawnPosition(spawnPosition: [number, number]): void {
    this.spawnPosition = spawnPosition;
    this.spawnPositionRect?.destroy();
    this.spawnPositionRect = this.add.rectangle(
      spawnPosition[0] * TILE_SIZE_PX + TILE_SIZE_PX / 2,
      spawnPosition[1] * TILE_SIZE_PX + TILE_SIZE_PX / 2,
      TILE_SIZE_PX * 0.5,
      TILE_SIZE_PX * 0.5
    );
    this.spawnPositionRect.setFillStyle(0x8888ff, 1);
  }

  // Events
  public onTileSelection(callback: (tileNumber: number) => void): void {
    this.events.on(SELECTED_TILE_CHANGED_EVENT_NAME, callback);
  }

  public onEditorCreated(callback: () => void): void {
    this.events.on(EDITOR_CREATED_EVENT_NAME, callback);
  }

  public onSpawnPlaced(callback: (spawnPosition: [number, number]) => void): void {
    this.events.on(NEW_SPAWN_EVENT_NAME, callback);
  }
}

const editorConfig: Phaser.Types.Core.GameConfig = {
  type: Phaser.WEBGL,
  width: "100%",
  height: "100%",
  parent: "board",
  scale: {
    mode: Phaser.Scale.FIT,
    height: "100%"
  },
  backgroundColor: "#000000",
  pixelArt: true,
  physics: {
    default: "arcade",
    arcade: { gravity: { y: 0 } }
  },
  scene: EditorScene
};

export { editorConfig, EditorScene };
