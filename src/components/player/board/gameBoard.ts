/**
 * @licence
 * Copyright 2022-2022 - RPG of the Apocalypse
 * Licensed under MIT or any later version
 * Refer to the LICENSE file included.
 */

import { Map, DefaultMap, GameUpdate, Direction } from "@/services/backends/world";
import { knownEntities, DisplayedEntity } from "@/services/editor/entities";
import Phaser from "phaser";
import { tilesets, tileCoordToTileIndex } from "../../../services/editor/tilesets";// Tileset parameters
import { v4 as uuidv4 } from "uuid";

// Tileset parameters
const TILESET = tilesets.tiles;
const ENTITY_TILESET = tilesets.entities;
const TILE_SIZE_PX = TILESET.imageSize[0];
const EMPTY_TILE_INDEX = tileCoordToTileIndex(0, 4, TILESET);

// Game parameters
const CAMERA_ROUND_PIXEL = false;
// const CAMERA_SPEED = 6;

// Map parameters
const NUMBER_OF_LAYERS = 5;
const playerEntity = knownEntities.player;
if (!playerEntity) throw Error("The player entity hasn't been found");

// Events
const BOARD_CREATED_EVENT_NAME = "boardCreated";
const PLAYER_MOVE_EVENT_NAME = "playerMove";

// Scene
class BoardScene extends Phaser.Scene {
  private MAP_SIZE = [ 25, 20 ];
  private map?: Phaser.Tilemaps.Tilemap;
  private layers: Phaser.Tilemaps.TilemapLayer[] = [];
  private player?: DisplayedEntity;
  private movementCursors?: Phaser.Types.Input.Keyboard.CursorKeys;
  private entities: DisplayedEntity[] = [];

  constructor() {
    super("BoardScene");
  }

  preload(): void {
    // Load tilesets
    Object.values(tilesets).forEach(tileset => {
      this.load.spritesheet(tileset.name, tileset.path, {
        frameWidth: tileset.imageSize[0],
        frameHeight: tileset.imageSize[1]
      });
    });
  }

  create(): void {
    // Init Camera, controls and animations,
    // the map will be loaded later

    // Adding the player
    const playerSprite = this.add.sprite(0, 0,
      playerEntity.tileset.name,
      playerEntity.imageTilesetPosition
    );
    playerSprite.visible = false; // Will be visible when the map is loaded
    playerSprite.originX = 0;

    // Camera
    this.cameras.main.startFollow(playerSprite, CAMERA_ROUND_PIXEL, 0.05, 0.05);
    this.cameras.main.setZoom(4);
    this.cameras.main.setBackgroundColor("#000000");

    const idleAnimation = playerSprite.anims.create({
      key: "idle",
      frames: this.anims.generateFrameNumbers(ENTITY_TILESET.name, {
        frames: playerEntity.idleAnimationImagesPosition
      }),
      frameRate: 8,
      repeat: -1
    });

    const runningAnimation = playerSprite.anims.create({
      key: "running",
      frames: this.anims.generateFrameNumbers(ENTITY_TILESET.name, {
        frames: playerEntity.runningAnimationImagesPosition
      }),
      frameRate: 8,
      repeat: -1
    });

    if (idleAnimation && runningAnimation) {
      this.player = {
        ...playerEntity,
        id: "player",
        x: 0,
        y: 0,
        sprite: playerSprite,
        idleAnimation: idleAnimation,
        runningAnimation: runningAnimation
      };
    }

    // Keyboard movement
    this.movementCursors = this.input.keyboard.createCursorKeys();

    this.input.keyboard.on("keydown-UP", () => {
      this.events.emit(PLAYER_MOVE_EVENT_NAME, Direction.North);
    });
    this.input.keyboard.on("keydown-DOWN", () => {
      this.events.emit(PLAYER_MOVE_EVENT_NAME, Direction.South);
    });
    this.input.keyboard.on("keydown-LEFT", () => {
      this.events.emit(PLAYER_MOVE_EVENT_NAME, Direction.West);
    });
    this.input.keyboard.on("keydown-RIGHT", () => {
      this.events.emit(PLAYER_MOVE_EVENT_NAME, Direction.East);
    });

    this.events.emit(BOARD_CREATED_EVENT_NAME);
  }

  private resetMap(): void {
    // Creating an empty map
    this.map = this.make.tilemap({
      tileWidth: TILE_SIZE_PX,
      tileHeight: TILE_SIZE_PX,
      width: this.MAP_SIZE[0],
      height: this.MAP_SIZE[1]
    });

    // Load tiles and reset layers
    const tiles = this.map.addTilesetImage(
      TILESET.name,
      undefined,
      TILE_SIZE_PX,
      TILE_SIZE_PX,
      0,
      0
    );

    // Reset layers
    this.layers.forEach(layer => {
      layer.destroy();
    });

    this.layers = [];
    for (let i = 0; i < NUMBER_OF_LAYERS; i++) {
      this.layers.push(this.map.createBlankLayer(`layer${i}`, tiles));
      // Fill the layer with empty tiles
      this.layers[i].fill(EMPTY_TILE_INDEX, 0, 0, this.MAP_SIZE[0], this.MAP_SIZE[1]);
      this.layers[i].setDepth(i + 1);
    }

    // Reset entities
    this.entities.forEach(entity => {
      entity.sprite.destroy();
      entity.idleAnimation?.destroy();
      entity.runningAnimation?.destroy();
    });
    this.entities = [];
  }

  private randomizeALittle(val: number, maxAddedRandom = 0.2): number {
    return val + (Math.random() - 0.5) * maxAddedRandom;
  }

  private moveEntity(entity: DisplayedEntity, x: number, y: number, teleport = false): void {
    // Entity orientation
    if (x > entity.x) entity.sprite.flipX = false;
    if (x < entity.x) entity.sprite.flipX = true;

    // Update coordinates
    entity.x = x;
    entity.y = y;

    // Move the entity
    if (teleport) {
      entity.sprite.x = x * TILE_SIZE_PX;
      entity.sprite.y = y * TILE_SIZE_PX - (entity.imagePxYOffset ?? 0);
    } else {
      // Smooth movement
      this.tweens.add({
        targets: entity.sprite,
        x: this.randomizeALittle(entity.x) * TILE_SIZE_PX,
        y: this.randomizeALittle(entity.y) * TILE_SIZE_PX - (entity.imagePxYOffset ?? 0),
        duration: 150,
        ease: "Power3"
      });
    }
    // The depth start at 3 because the entities are above the layers 1 2 and 3
    // Then it goes from 3.0 to 4.0 depending on the sprite y coord
    // so entities that are above are displayed below
    entity.sprite.setDepth(3 + entity.y / this.MAP_SIZE[1]);
  }

  private movePlayer(x: number, y: number, teleport = false): void {
    if (this.player) {
      // Move
      this.moveEntity(this.player, x, y, teleport);
    }
  }

  private initPlayerSprite(): void {
    if (this.player) {
      this.player.sprite.visible = true;
      this.cameras.main.centerOn(this.player.x * TILE_SIZE_PX, this.player.y * TILE_SIZE_PX);
    }
  }

  update(): void {
    // Update the animation last and give left/right animations precedence over up/down animations
    if (this.movementCursors && this.player) {
      if (this.movementCursors.left.isDown ||
        this.movementCursors.right.isDown ||
        this.movementCursors.up.isDown ||
        this.movementCursors.down.isDown
      ) {
        this.player.sprite.anims.play("running", true);
      } else {
        this.player.sprite.anims.play("idle", true);
      }
    }
  }

  // Public
  public setMap(map: Map | DefaultMap): void {
    // Reset the map
    this.MAP_SIZE = [ map.tileMap[0].length, map.tileMap.length ];
    this.resetMap();
    this.applyMap(map);
  }

  private applyMap(map: Map | DefaultMap): void {
    // Set tileMap
    map.tileMap.forEach((row, y) => {
      row.forEach((layerTiles, x) => {
        layerTiles.forEach((layerTile, layerNumber) => {
          this.layers[layerNumber].putTileAt(layerTile, x, y);
        });
      });
    });

    // Set entities
    if (map.entities) {
      map.entities.forEach(entity => {
        if (entity.x < this.MAP_SIZE[0] && entity.y < this.MAP_SIZE[1]) {
          this.placeEntity(entity.x, entity.y, entity.name);
        }
      });
    }

    // Set spawn position
    if ("spawnPosition" in map) {
      this.movePlayer(map.spawnPosition[0], map.spawnPosition[1], true);
      this.initPlayerSprite();
    }
  }

  private placeEntity(x: number, y: number, entityName: string): void {
    // Get the entity that we want to place
    const knownEntity = knownEntities[entityName];
    if (knownEntity === undefined) return;

    // Create the entitySprite
    const entitySprite = this.physics.add.sprite(0, 0,
      knownEntity.tileset.name,
      knownEntity.imageTilesetPosition
    );

    // Set the sprite offset,
    // 0 x 0 if the entity sprite is tile sized
    // 0 x 0.5 if the entity sprite if two tiles high
    entitySprite.setOrigin(0, (knownEntity.tileset.imageSize[1] / TILE_SIZE_PX - 1) / 2);

    // Create the animations
    let idleAnimation, runningAnimation;
    if (knownEntity.idleAnimationImagesPosition) {
      idleAnimation = entitySprite.anims.create({
        key: "idle",
        frames: this.anims.generateFrameNumbers(knownEntity.tileset.name, {
          frames: knownEntity.idleAnimationImagesPosition
        }),
        frameRate: 8,
        repeat: -1
      });
      entitySprite.anims.play("idle", true);
    }
    if (knownEntity.runningAnimationImagesPosition) {
      runningAnimation = entitySprite.anims.create({
        key: "running",
        frames: this.anims.generateFrameNumbers(knownEntity.tileset.name, {
          frames: knownEntity.runningAnimationImagesPosition
        }),
        frameRate: 8,
        repeat: -1
      });
    }

    const entity: DisplayedEntity = {
      ...knownEntity,
      id: uuidv4(),
      name: entityName,
      x: 0,
      y: 0,
      sprite: entitySprite,
      idleAnimation: idleAnimation || undefined,
      runningAnimation: runningAnimation || undefined
    };

    // Deal with entity offset and sprite depth
    this.moveEntity(entity, x, y, true);

    this.entities.push(entity);
  }

  public updateGame(gameUpdate: GameUpdate): void {
    // Update the player
    this.movePlayer(gameUpdate.playerPosition[0], gameUpdate.playerPosition[1]);
    // TODO V2: Entity update
  }

  // Events
  public onBoardCreated(callback: () => void): void {
    this.events.on(BOARD_CREATED_EVENT_NAME, callback);
  }

  public onPlayerMove(callback: (direction: Direction) => void): void {
    this.events.on(PLAYER_MOVE_EVENT_NAME, callback);
  }
}

const gameConfig: Phaser.Types.Core.GameConfig = {
  type: Phaser.WEBGL,
  width: "100%",
  height: "100%",
  parent: "board",
  scale: {
    mode: Phaser.Scale.FIT,
    height: "100%"
  },
  backgroundColor: "#2d2d2d",
  pixelArt: true,
  physics: {
    default: "arcade",
    arcade: { gravity: { y: 0 } }
  },
  scene: BoardScene
};

export { gameConfig, BoardScene };
