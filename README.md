# Client

<p align="center">The official client of the RPG of the Apocalypse</p>


## Requirements

* NodeJS (version: `16.13.0`)

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint:fix
```
