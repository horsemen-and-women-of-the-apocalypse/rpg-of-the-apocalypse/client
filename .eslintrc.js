module.exports = {
  root: true,
  env: {
    node: true
  },
  extends: [
    "plugin:vue/essential",
    "@vue/standard",
    "@vue/typescript/recommended"
  ],
  parserOptions: {
    ecmaVersion: 2020
  },
  rules: {
    "no-console": process.env.NODE_ENV === "production" ? "warn" : "off",
    "no-debugger": process.env.NODE_ENV === "production" ? "warn" : "off",
    "array-bracket-spacing": [ "error", "always" ],
    "object-curly-spacing": [ "error", "always" ],
    "linebreak-style": [ "error", "unix" ],
    "space-before-function-paren": [ "error", "never" ],
    indent: [ "error", 2, { SwitchCase: 1 } ],
    quotes: [ "error", "double" ],
    semi: [ "error", "always" ],
    "no-unused-expressions": 0
  },
  overrides: [
    {
      files: [
        "**/__tests__/*.{j,t}s?(x)",
        "**/tests/unit/**/*.spec.{j,t}s?(x)"
      ],
      env: {
        mocha: true
      }
    },
    {
      files: [ "*.vue" ],
      rules: { "vue/script-indent": "off" }
    }
  ]
};
